using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkTransformGrab : NetworkTransformBase
{
    public Transform targetTransform;
    protected override Transform targetComponent => targetTransform;

    [Server]
    public void TeleportOnServer(Vector3 destination)
    {
        OnTeleport(destination);

        RpcTeleport(destination);
    }

    protected override void OnValidate() 
    {
        base.OnValidate();
        if(targetTransform == null)
            targetTransform = transform;
    }
    
}
