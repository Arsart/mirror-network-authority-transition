using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MyNetworkTransform : NetworkBehaviour
{
    public bool useAutorithy = true;

    [SyncVar(hook =nameof(OnChangePosition))]
    protected Vector3 currentPosition;
    [SyncVar(hook =nameof(OnChangeRotation))]
    protected Quaternion currentRotation;

    protected bool IsClientWithAuthority
    {
        get => useAutorithy && hasAuthority && isClient;
    }


    protected double lastSyncServer;
    protected double lastSyncClient;

    protected Transform myTransform;

    private void Awake() {
        myTransform = transform;
    }

    private void Update() {
        if(isServer)
        {
            UpdateServer();
        }
        else if(isClient)
        {
            UpdateClient();
        }
    }

    [Server]
    protected void UpdateServer()
    {
        //Will only force sync if don't use authority or there's no client with authority
        if(!useAutorithy || connectionToClient == null || IsClientWithAuthority)
        {
            currentPosition = myTransform.position;
            currentRotation = myTransform.rotation;
        }
    }

    [Client]
    protected void UpdateClient()
    {
        if(IsClientWithAuthority && Time.timeAsDouble > lastSyncClient + syncInterval)
        {
            lastSyncClient = Time.timeAsDouble;

            CmdSendNewTransform(myTransform.position, myTransform.rotation);
        }
    }

    [Command]
    protected void CmdSendNewTransform(Vector3 position, Quaternion rotation)
    {
        myTransform.position = position;
        myTransform.rotation = rotation;

        currentPosition = myTransform.position;
        currentRotation = myTransform.rotation;
    }


    protected void OnChangePosition(Vector3 oldPos, Vector3 newPos)
    {
        if(!hasAuthority)
            myTransform.position = newPos;
    }

    protected void OnChangeRotation(Quaternion oldRot, Quaternion newRot)
    {
        if(!hasAuthority)
            myTransform.rotation = newRot;
    }
    
 
}
