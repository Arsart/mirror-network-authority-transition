using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Grabber : NetworkBehaviour
{
    public float distance= 20;

    protected Camera myCamera;
    protected Grabbable currentGrabbabble = null;

    private void Start() {
        myCamera = GetComponentInChildren<Camera>();
        myCamera.gameObject.SetActive(isLocalPlayer);
    }

    void Update()
    {
        if(!isLocalPlayer) return;

        //On click left button mouse
        if(Input.GetMouseButtonDown(0))
        {
            Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(mouseRay, out hit, distance))
            {
                Grabbable toGrab = hit.collider.GetComponentInParent<Grabbable>();
                if(toGrab != null && toGrab.canBeGrabbed)
                {
                    Grab(toGrab);
                }
            }
        }
        //release
        else if(Input.GetMouseButtonUp(0))
        {
            Release();
        }

        if(currentGrabbabble != null)
        {
            //Mouse position
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(mouseRay, out hit, distance, LayerMask.GetMask("Ground")))
            {
                Vector3 mousePosition = hit.point;
                currentGrabbabble.transform.position = mousePosition;
            }
        }
    }


    void Grab(Grabbable toGrab)
    {
        currentGrabbabble = toGrab;
        CmdGrab(currentGrabbabble.GetComponent<NetworkIdentity>(), currentGrabbabble.transform.position);
    }

    [Command]
    void CmdGrab(NetworkIdentity toGrab, Vector3 position)
    {
        toGrab.AssignClientAuthority(connectionToClient);
        toGrab.GetComponent<Grabbable>().canBeGrabbed = false;
        NetworkTransformGrab nt = toGrab.GetComponent<NetworkTransformGrab>();
        if(nt != null)
        {
            nt.TeleportOnServer(position);
        }
    }

    void Release()
    {
        if(currentGrabbabble != null)
        {
            CmdRelease(currentGrabbabble.GetComponent<NetworkIdentity>(),currentGrabbabble.transform.position);
            currentGrabbabble = null;
        }
    }

    [Command]
    void CmdRelease(NetworkIdentity toRelease, Vector3 position)
    {
        toRelease.RemoveClientAuthority();
        toRelease.GetComponent<Grabbable>().canBeGrabbed = true;

        NetworkTransformGrab nt = toRelease.GetComponent<NetworkTransformGrab>();
        if(nt != null)
        {
            nt.TeleportOnServer(position);
        }
    }

    void LocalRelease(NetworkIdentity toRelease)
    {
        toRelease.RemoveClientAuthority();
        toRelease.GetComponent<Grabbable>().canBeGrabbed = true;

    }
}
