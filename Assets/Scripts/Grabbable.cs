using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Grabbable : NetworkBehaviour
{
    [SyncVar]
    public bool canBeGrabbed = false;

    public Transform pointA;
    public Transform pointB;

    public float speed = 5;
    public bool moveOnServer = true;

    protected Coroutine walkCoroutine = null;

    private void Update() {
        if(isServer && moveOnServer)
        {
            if(connectionToClient == null)
            {
                if(walkCoroutine == null)
                {
                    walkCoroutine = StartCoroutine(WalkToOtherPoint());
                }
            }
            else
            {
                if(walkCoroutine != null)
                {
                    StopCoroutine(walkCoroutine);
                    walkCoroutine = null;
                }
            }
        }
    }

    IEnumerator WalkToOtherPoint()
    {
        bool gointToB = true;
        Vector3 target;
        while(true)
        {
            target = (gointToB) ? pointB.position : pointA.position;

            Vector3 dif = target - transform.position;
            while(dif.magnitude > Mathf.Epsilon)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
                yield return null;
                dif = target - transform.position;
            }
            gointToB = !gointToB;
        }
    }
}
